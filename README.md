Getting a ticket table from Gitlab
==================================

Usage: open the file gitlab-ticket-table.html in your browser like so:

 file:///home/yourusername/gitlab-ticket-table.html/privateToken=REPLACEi-WITH-GITLAB-PRIVATE-TOKEN&projectId=REPLACE-WITH-PROJECT-ID

inserting your private token (Gitlab -> Profile settings -> Account) and project id (either an integer or NAMESPACE/PROJECT-NAME e.g. raisoman%2Fgitlab-sandbox) where appropriate.

